<?php

return [
    'setup_routes'=>true,
    'setup_event_route'=>true,
    'setup_date_route'=>true,
    'setup_subdate_route'=>true,
    
    't_delete_dates'=> 'kda-event-admin::event.message_delete_dates',
    'alert_dates_cleared'=> 'kda-event-admin::event.alert_dates_cleared',
    'alert_dates_generated'=>'kda-event-admin::event.alert_dates_generated',

    'allowed_events_entities'=>[

    ],
    'entity_data_source'=>'admin/event/fetch/related',
    'allow_generate_dates'=>false
];