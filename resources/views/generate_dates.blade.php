@if ($crud->hasAccess('generate_dates'))
        <a href="{{ url($crud->route.'/'.$entry->getKey().'/generate_dates') }}"  class="btn btn-sm btn-link"><span class="la la-sync"></span> &nbsp; Regénerer les dates</a>
@endif