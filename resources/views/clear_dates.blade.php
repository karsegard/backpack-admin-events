@php
$url = url($crud->route . '/clear_dates?' . $crud->getSubControllerQuerystring());
@endphp
@if ($crud->hasAccess('clear_dates'))
    <a href="javascript:void(0)" onclick="deleteDates(this)" class="btn  btn-danger"><span class="la la-sync"></span> &nbsp;
        Effacer les dates</a>
@endif


{{-- Button Javascript --}}
{{-- - used right away in AJAX operations (ex: List) --}}
{{-- - pushed to the end of the page, after jQuery is loaded, for non-AJAX operations (ex: Show) --}}
@push('after_scripts')
    @once
        <script>
            function deleteDates(button) {
                // ask for confirmation before deleting an item
                // e.preventDefault();
                var route = $(button).attr('data-route');

                swal({
                    title: "{!! trans('backpack::base.warning') !!}",
                    text: "{!! trans(config('kda.eventadmin.t_delete_dates')) !!}",
                    icon: "warning",
                    buttons: ["{!! trans('backpack::crud.cancel') !!}", "{!! trans('backpack::crud.delete') !!}"],
                    dangerMode: true,
                }).then((value) => {
                    if (value) {
                        window.location.href = "{!! $url !!}"
                    }
                });

            }
        </script>
    @endonce
@endpush
