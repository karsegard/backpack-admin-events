@if ($crud->hasAccess('generate_dates'))
        <a href="{{ url($crud->route.'/generate_dates?'.$crud->getSubControllerQuerystring()) }}"  class="btn  btn-primary"><span class="la la-sync"></span> &nbsp; Regénerer les dates</a>
@endif