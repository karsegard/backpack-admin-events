<?php

namespace KDA\Events\Admin\Http\Controllers\Admin;

use KDA\Events\Admin\Http\Requests\DateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class NavigationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DateCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\KDA\Events\Admin\Models\Date::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/date');
        CRUD::setEntityNameStrings('date', 'dates');
        $this->loadPermissions('date');
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', config('kda.navadmin.max_depth', 2));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        
        CRUD::addColumn([
            'name'  => 'date', // The db column name
            'label' => 'Date', // Table column heading
            'type'  => 'datetime',
             'format' => 'D.MM.Y', // use something else than the base.default_datetime_format config value
        ]);

        CRUD::addColumn([
            'name'  => 'date_end', // The db column name
            'label' => 'Date de fin', // Table column heading
           
      
            'format' => 'D.MM.Y', // use something else than the base.default_datetime_format config value
        ]);

        CRUD::addColumn([
            'name'=>'time',
            'type'=>'datetime',
            'format'=>'H:mm'
        ]);

        CRUD::addColumn([
            'name'  => 'status', // The db column name
        ]);

      
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DateRequest::class);


    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
