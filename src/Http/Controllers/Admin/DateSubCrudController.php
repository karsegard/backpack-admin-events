<?php

namespace KDA\Events\Admin\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use  KDA\Events\Admin\Models\Event;
use  KDA\Events\Admin\Models\Date as DateModel;
use Illuminate\Support\Facades\Route;

class DateSubCrudController extends DateCrudController
{
    use \KDA\Backpack\Subcontroller\Traits\SubCrudController;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        parent::setup();
        CRUD::setRoute(config('backpack.base.route_prefix') . '/subdate');

        $this->setParentController([
            'route' => 'event',
            'clause' => 'forEvent',
            'key' => 'event_id'
        ]);

        $controllerSettings = $this->crud->getSubControllerCurrentValues();
        $event = $controllerSettings ? $controllerSettings['args']['event_id'] : NULL;
        $this->event = Event::find($event);
        $this->setupSubController();
    }
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        parent::setupListOperation();
        $this->crud->setHeading('Dates');
        $this->crud->setSubheading('pour l\'évenement ' . $this->event->title);
        $this->crud->removeAllFilters();

        $this->setupSubControllerListOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        parent::setupCreateOperation();

        CRUD::addField([   // date_picker
            'name'  => 'date',
            'type'  => 'date_picker',
            'label' => 'Date',
            'default' => date("Y-m-d"),
            // optional:
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd/mm/yyyy',
                'language' => 'fr'
            ],
        ]);
        CRUD::addField([   // date_picker
            'name'  => 'date_end',
           // 'type'  => 'date_picker',
            'label' => 'Date de fin',
            'hint'=>'Si l\'évenement se passe sur plusieurs jours',
            'type'  => 'null_date_picker',
            'allows_null' => true,
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            // optional:
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd/mm/yyyy',
                'language' => 'fr'
            ],
        ]);
        CRUD::addField([
            'name' => 'time_start',
            'label' => 'heure',
            'type' => 'datetime_picker',
            'default' => $this->event->time_start,
            'datetime_picker_options' => [
                'format' => 'LT',
                'language' => 'fr'
            ],
            'wrapper' => ['class' => 'col-md-6']
        ]);
        CRUD::addField([
            'name' => 'time_end',
            'label' => 'heure de fin',
            'type' => 'datetime_picker',
            'default' => $this->event->time_end,
            'datetime_picker_options' => [
                'format' => 'LT',
                'language' => 'fr'
            ],
            'wrapper' => ['class' => 'col-md-6']
        ]);
        CRUD::field('status')->type('enum');
        $this->setupSubControllerCreateOperation();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        $this->setupSubControllerUpdateOperation();
    }


    protected function setupGenerateDatesRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/generate_dates', [
            'as'        => $routeName . '.generate_dates',
            'uses'      => $controller . '@generate_dates',
            'operation' => 'generate_dates',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupGenerateDatesDefaults()
    {
        
        if(!config('kda.eventadmin.allow_generate_dates',false)){
            $this->crud->denyAccess('generate_dates');
        }else{
            $this->crud->allowAccess('generate_dates');
        }


        $this->crud->operation(['list'], function () {
            $this->crud->addButton('top', 'generate_dates', 'view', 'kda-event-admin::generate_dates_dates');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function generate_dates()
    {
        $this->crud->hasAccessOrFail('generate_dates');

        $entry =  $this->event;
        $entry->regenerateDates();

        \Alert::success(__(config('kda.eventadmin.alert_dates_generated')))->flash();

        return back();
    }

    protected function setupClearDatesRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/clear_dates', [
            'as'        => $routeName . '.clear_dates',
            'uses'      => $controller . '@clear_dates',
            'operation' => 'clear_dates',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupClearDatesDefaults()
    {
        $this->crud->allowAccess('clear_dates');



        $this->crud->operation(['list'], function () {
            $this->crud->addButton('top', 'clear_dates', 'view', 'kda-event-admin::clear_dates');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function clear_dates()
    {
        $this->crud->hasAccessOrFail('clear_dates');

        DateModel::where('event_id', $this->event->id)->delete();
        \Alert::success(__(config('kda.eventadmin.alert_dates_cleared')))->flash();

        return back();
    }
}
