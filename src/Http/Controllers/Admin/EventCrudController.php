<?php

namespace KDA\Events\Admin\Http\Controllers\Admin;

use KDA\Events\Admin\Http\Requests\EventRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use KDA\Events\Models\Date as DateModel;

/**
 * Class NavigationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EventCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    use \KDA\Backpack\Subcontroller\Operations\ManageSubControllersOperation;

    public function fetchRelated()
    {
        $data = collect(request()->input('form'))->pluck('value', 'name');
        $model = $data['related_type'];
        return $this->fetch([
            'model' => $model,
            'query' => function ($model) {
                $search = request()->input('q') ?? false;
                if ($search) {
                    return $model->filteredWith($search);
                } else {
                    return $model;
                }
            },
        ]);
    }
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\KDA\Events\Admin\Models\Event::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/event');
        CRUD::setEntityNameStrings('event', 'events');
        $this->loadPermissions('event');

        $this->setupManageSubControllersOperation();
    }

    protected function setupManageSubControllersOperation()
    {
        $this->setSubControllers([
            'subdate' => [
                'key' => 'event_id',
                'label' => 'Gérer les dates',
            ]
        ], 'event');
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', config('kda.navadmin.max_depth', 2));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $allowed_entities = config('kda.eventadmin.allowed_events_entities');
        CRUD::column('id');
        CRUD::column('event_title')->label('Nom')->orderable(true)->orderLogic(function ($query, $column, $columnDirection) {
            return $query->orderBy('title',$columnDirection);
        });
        if (count($allowed_entities) > 0) {
            CRUD::addColumn([
                'name'=>'related_type',
                'type'=>'closure',
                'function' => function ($entry) use ($allowed_entities){
                    return $allowed_entities[$entry->related_type] ?? '';
                    }
                ,
            ]);
        }
        // CRUD::column('start_on');
        // CRUD::column('end_on');
        CRUD::addColumn(
            [
                // 1-n relationship
                'label' => "Dates", // Table column heading
                'type' => "relationship_count",
                'name' => 'dates', // the column that contains the ID of that connected entity;
                'suffix' => ''
            ]
        );
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $allowed_entities = config('kda.eventadmin.allowed_events_entities');
        $data_source = config('kda.eventadmin.entity_data_source');
        CRUD::setValidation(EventRequest::class);
        $entry= $this->crud->getCurrentEntry();
        $entityModel =  $entry?->related_type ??array_keys($allowed_entities)[0] ;
        if (count($allowed_entities) > 0) {
            CRUD::addField([   // select_from_array
                'name'        => 'related_type',
                'label'       => "Type",
                'type'        => count($allowed_entities) > 1 ?'select_from_array':'hidden',
                'options'     => $allowed_entities,
                'allows_null' => false,
                'default'     => array_keys($allowed_entities)[0],
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ]);
            CRUD::addField([
                "name" => 'related_id',
                'label'=>'Entité',
                'type' => 'select2_from_ajax',
                'data_source' => url($data_source),
                'method' => 'post',
                'entity' => false,
                'model'=>$entityModel,
                'attribute' => 'backpack_identifier',
                'placeholder' => 'Cherchez un élément',
                'include_all_form_fields' => true,
                'minimum_input_length' => 2,
                'dependencies'  => ['related_type']
            ]);
        }
        CRUD::field('title')->tab('Designation')->hint('Inutile si vous avez attribué un spectacle ou un évenement ');


        CRUD::addField([
            'name'  => ['start', 'end'],
            'label' => 'Début/Fin',
            'hint' => 'Spécifie dans quelle intervalle générer les dates',
            'type' => 'date_range',

            'date_range_options' => [
                'timePicker' => false,
                'todayBtn' => 'linked',
                'format'   => 'dd.mm.yyyy',
                'language' => 'fr'
            ],
            'tab' => 'Génération des dates',
            'wrapper' => ['class' => 'col-md-12']
        ]);
        CRUD::addField([
            'name' => 'time_start',
            'label' => 'heure de début',
            'hint' => 'L\'Heure de début des dates générées',
            'type' => 'datetime_picker',
            'datetime_picker_options' => [
                'format' => 'LT',
                'language' => 'fr'
            ],
            'tab' => 'Génération des dates',
            'wrapper' => ['class' => 'col-md-4']
        ]);
        CRUD::addField([
            'name' => 'time_end',
            'label' => 'heure de fin',
            'hint' => 'L\'Heure de fin des dates générées',
            'type' => 'datetime_picker',
            'datetime_picker_options' => [
                'format' => 'LT',
                'language' => 'fr'
            ],
            'tab' => 'Génération des dates',
            'wrapper' => ['class' => 'col-md-4']
        ]);

        CRUD::addField([   // repeatable
            'name'          => 'all_day',
            'label'         => 'Toute la journée',
            'type' => 'toggle-label',
            'hint' => 'Pas d\'horaire',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'tab' => 'Génération des dates',
            'wrapper' => ['class' => 'col-md-4']

        ]);

        CRUD::addField([   // repeatable
            'name'          => 'generate',
            'label'         => 'Générer des dates',
            'defaultValue' => false,
            'type' => 'toggle-label',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'tab' => 'Génération des dates',
            'wrapper' => ['class' => 'col-md-12']

        ]);
        CRUD::field('reccurence')->label('Récurrence')->type('enum')->tab('Génération des dates')->wrapper(['class' => 'col-md-6']);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {

        CRUD::removeField('related_id');
        $allowed_entities = config('kda.eventadmin.allowed_events_entities');
        $data_source = config('kda.eventadmin.entity_data_source');
        CRUD::setValidation(EventRequest::class);
        if (count($allowed_entities) > 0) {
            $entry = $this->crud->getCurrentEntry();
            $model = $entry && $entry->related ? get_class($entry->related) : NULL;
            CRUD::addField([
                "name" => 'related_id',
                'type' => 'select2_from_ajax',
                'data_source' => url($data_source),
                'method' => 'post',
                'entity' => false,
                'model'=>$model,
                'attribute' => 'backpack_identifier',
                'placeholder' => 'Cherchez un élément',
                'include_all_form_fields' => true,
                'minimum_input_length' => 2,
                'dependencies'  => ['related_type']
            ]);
        }
        $this->setupCreateOperation();
    }

    protected function setupGenerateDatesRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/generate_dates', [
            'as'        => $routeName . '.generate_dates',
            'uses'      => $controller . '@generate_dates',
            'operation' => 'generate_dates',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupGenerateDatesDefaults()
    {
        if(!config('kda.eventadmin.allow_generate_dates',false)){
            $this->crud->denyAccess('generate_dates');
        }else{
            $this->crud->allowAccess('generate_dates');
        }


        $this->crud->operation(['list', 'edit'], function () {
            $this->crud->addButton('line', 'generate_dates', 'view', 'kda-event-admin::generate_dates');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function generate_dates($id)
    {
        $this->crud->hasAccessOrFail('generate_dates');
        $id = $this->crud->getCurrentEntryId() ?? $id;


        $entry = $this->crud->getEntry($id);
        $entry->regenerateDates();


        \Alert::success(__(config('kda.eventadmin.alert_dates_generated')))->flash();

        return back();
    }


    public function store()
    {

        $response = $this->traitStore();

        $request = $this->crud->validateRequest();
        $data = $this->crud->getStrippedSaveRequest($request);
        if (isset($data['related_type']) && isset($data['related_id'])) {
            $model = $data['related_type'];
            $value = $data['related_id'];
            $entity = $model::find($value);
            $this->crud->entry->related()->associate($entity)->save();
        }
        return $response;
    }

    public function update()
    {

        $response = $this->traitUpdate();
        $request = $this->crud->validateRequest();
        $data = $this->crud->getStrippedSaveRequest($request);

        if (isset($data['related_type']) && isset($data['related_id'])) {

            $model = $data['related_type'];
            $value = $data['related_id'];
            $entity = $model::find($value);
            $this->crud->entry->related()->associate($entity)->save();
        }else{
            $this->crud->entry->related()->dissociate()->save();
            
        }
        return $response;
    }
}
