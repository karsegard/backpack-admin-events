<?php

namespace KDA\Events\Admin;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasTranslations;
    use \KDA\Laravel\Traits\HasDynamicSidebar;
    use \KDA\Laravel\Traits\HasDumps;
    protected $dumps=[
        'events',
        'dates',
    
    ];
    protected $packageName='kda-event-admin';
    protected $viewNamespace = 'kda-event-admin';
    protected $publishViewsTo = 'vendor/kda/backpack/event';
    protected $sidebars = [
        [
            'label'=>'Events',
            'route'=> 'event',
            'behavior'=>'disable',
            'icon'=>'la-calendar'
        ],
       
    ];
    protected $routes = [
        'backpack/events.php'
    ];
    protected $configs = [
        'kda/eventadmin.php'=> 'kda.eventadmin'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
