<?php

namespace KDA\Events\Admin\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends \KDA\Events\Models\Event
{
    use CrudTrait;
    
}
