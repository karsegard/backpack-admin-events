<?php
Route::group([
    'namespace'  => 'KDA\Events\Admin\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    if (config('kda.eventadmin.setup_routes', true)) {

        if (config('kda.eventadmin.setup_date_route', true)) {
            Route::crud('date', 'DateCrudController');
        }
        if (config('kda.eventadmin.setup_subdate_route', true)) {
            Route::crud('subdate', 'DateSubCrudController');
        }
        if (config('kda.eventadmin.setup_event_route', true)) {
            Route::crud('event', 'EventCrudController');
        }
    }
});
